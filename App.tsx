/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';

// import {
//   Colors,
//   DebugInstructions,
//   Header,
//   LearnMoreLinks,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Register from './src/pages/Register';
import Login from './src/pages/Login';
import Main from './src/pages/Main';
import Home from './src/pages/Home';
import Modrate from './src/pages/Modrate';
import Splash from './src/pages/Splash';
const Stack = createNativeStackNavigator();

const App = () => (
  <NavigationContainer>
    <Stack.Navigator>
    <Stack.Screen
        options={{headerShown: false}}
        name={'Splash'}
        component={Splash}
      />
      <Stack.Screen
        options={{headerShown: false,animation: 'slide_from_right'}}
        name={'Register'}
        component={Register}
      />
      <Stack.Screen
        options={{headerShown: false,animation: 'slide_from_right'}}
        name={'Login'}
        component={Login}
      />
      <Stack.Screen
        options={{headerShown: false,animation: 'slide_from_right'}}
        name={'Main'}
        component={Main}
      />
      <Stack.Screen
        options={{headerShown: false,animation: 'slide_from_right'}}
        name={'Home'}
        component={Home}
      />
      <Stack.Screen
        options={{headerShown: false,animation: 'slide_from_right'}}
        name={'Modrate'}
        component={Modrate}
      />
    </Stack.Navigator>
  </NavigationContainer>
);

export default App;
