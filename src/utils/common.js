import Snackbar from 'react-native-snackbar';

function showSnack(msg) {
  Snackbar.show({
    text: '' + msg,
    duration: Snackbar.LENGTH_SHORT,
  });
}
const strongRegex = new RegExp(
  '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$',
);
const AppLog = (rnlogtitle, rnlogtext) => {
  if (__DEV__) {
    //log in dev build
    console.log(rnlogtitle, '' + rnlogtext);
  } else {
    //log in product build
  }
};
export {AppLog, showSnack, strongRegex};
