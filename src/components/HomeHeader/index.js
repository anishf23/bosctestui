import React from 'react';
import {Text, Image, TouchableOpacity, View} from 'react-native';
import styles from './styles';
const HomeHeader = ({
  viewStyle,
  headerTitle,
  headerLeftIcon,
  headerRightIcon,
  onLeftIconPress,
  onRightIconPress,
  isImage,
}) => (
  <View style={[styles.mainContainer, viewStyle]}>
    <TouchableOpacity onPress={onLeftIconPress}>
      <Image
        style={[styles.headerIcon]}
        resizeMode="contain"
        source={headerLeftIcon}
      />
    </TouchableOpacity>

    <Text style={styles.headerText}>{''}</Text>

    {isImage ? (
      <TouchableOpacity onPress={onRightIconPress}>
        <Image
          style={[styles.headerRightIcon]}
          resizeMode="contain"
          source={headerRightIcon}
        />
      </TouchableOpacity>
    ) : (
      <Text style={styles.rightText}>{'Account \nSetup'}</Text>
    )}
  </View>
);

export default HomeHeader;

HomeHeader.defaultProps = {
  headerTitle: 'title',
  viewStyle: {},
  headerLeftIcon: {},
  headerRightIcon: '',
  onLeftIconPress: () => {},
  onRightIconPress: () => {},
};
