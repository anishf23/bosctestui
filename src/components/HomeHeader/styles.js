import {StyleSheet, Platform} from 'react-native';
import {FontSizes, Colors, wp, hp} from '../../theme';

const styles = StyleSheet.create({
  mainContainer: {
    height: hp(10),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: Platform.OS === 'android' ? 23 : 10,
  },
  headerText: {
    flex: 1,
    color: Colors.white,
    fontSize: FontSizes.xxLarge,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  headerIcon: {
    width: wp(7),
    height: hp(7),
    alignItems: 'center',
    margin: 10,
  },
  headerRightIcon: {
    width: wp(15),
    height: hp(15),
    alignItems: 'center',
    margin: 10,
  },
  rightText: {
    color: Colors.theme,
    fontSize: FontSizes.medium,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: '600',
    marginTop: 10,
  },
});

export default styles;
