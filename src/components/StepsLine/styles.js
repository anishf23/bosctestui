import {StyleSheet, Platform} from 'react-native';
import {FontSizes, Colors, wp, hp} from '../../theme';

const styles = StyleSheet.create({
  LineView: {
    width: 60,
    height: 4,
    marginEnd: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    borderRadius: 10,
    marginTop: 20,
  },
});

export default styles;
