import React from 'react';
import {View} from 'react-native';
import {Colors} from '../../theme';
import styles from './styles';

const StepsLine = ({selectUnSelectColor}) => (
  <View style={[styles.LineView, {backgroundColor: selectUnSelectColor}]} />
);

export default StepsLine;

StepsLine.defaultProps = {
  selectUnSelectColor: Colors.unSelectedTab,
};
