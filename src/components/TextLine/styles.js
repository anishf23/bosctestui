import {StyleSheet, Platform} from 'react-native';
import {FontSizes, Colors} from '../../theme';

const styles = StyleSheet.create({
  LineView: {
    width: 70,
    height: 4,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.investButtonBgColor,
    marginTop: 5,
  },
  mainLineView: {
    width: 120,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLargeBold: {
    color: Colors.theme,
    fontSize: FontSizes.xxLarge,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: '600',
  },
});

export default styles;
