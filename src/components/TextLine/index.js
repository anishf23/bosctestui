import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {Colors} from '../../theme';
import styles from './styles';

const TextLine = ({
  selectUnSelectColor,
  title,
  selectedTextColor,
  linesize,
  linePress,
  isSelected,
}) => (
  <TouchableOpacity onPress={linePress} style={styles.mainLineView}>
    <Text
      style={[
        styles.textLargeBold,
        // eslint-disable-next-line react-native/no-inline-styles
        {color: selectedTextColor, opacity: isSelected ? 1 : 0.4},
      ]}>
      {title}
    </Text>
    <View
      style={[
        styles.LineView,
        {width: linesize, backgroundColor: selectUnSelectColor},
      ]}
    />
  </TouchableOpacity>
);

export default TextLine;

TextLine.defaultProps = {
  selectUnSelectColor: Colors.unSelectedTab,
  linePress: () => {},
};
