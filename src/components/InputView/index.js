import React from 'react';
import {Text, TextInput, Image, View} from 'react-native';
import {Colors} from '../../theme';
import styles from './styles';
const InputView = ({
  txt,
  viewStyle,
  editable,
  onChangeText,
  keyboardType,
  secureTextEntry,
  reference,
  onSubmitEditing,
  isImage,
  RightIcon,
  placeholderColor,
}) => (
  <View style={[styles.mainContainer, viewStyle]}>
    <TextInput
      style={styles.textInput}
      editable={editable}
      placeholder={txt}
      placeholderTextColor={placeholderColor}
      onChangeText={onChangeText}
      keyboardType={keyboardType}
      secureTextEntry={secureTextEntry}
      ref={reference}
      onSubmitEditing={onSubmitEditing}
    />

    <Text style={styles.headerText}>{''}</Text>

    {isImage ? (
      <Image
        style={[styles.headerIcon]}
        resizeMode="contain"
        source={RightIcon}
      />
    ) : null}
  </View>
);

export default InputView;

InputView.defaultProps = {
  txt: 'title',
  viewStyle: {},
  RightIcon: '',
};
