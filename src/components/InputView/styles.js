import {StyleSheet, Platform} from 'react-native';
import {FontSizes, Colors, wp, hp} from '../../theme';

const styles = StyleSheet.create({
  mainContainer: {
    height: hp(7),
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: Colors.white,
    borderColor: Colors.borderColor,
    borderWidth: 0.9,
    borderRadius: 5,
    opacity: 0.5,
    marginTop: Platform.OS === 'android' ? 23 : 10,
  },
  headerText: {
    flex: 1,
    color: Colors.white,
    fontSize: FontSizes.xxLarge,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  headerIcon: {
    width: wp(4),
    height: hp(4),
    alignItems: 'center',
    justifyContent: 'center',
    marginStart: 10,
    marginEnd: 20,
    marginTop: 10,
    marginBottom: 10,
  },
  rightText: {
    color: Colors.theme,
    fontSize: FontSizes.medium,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: '600',
    marginTop: 10,
  },
  textInput: {
    width: wp(70),
    marginStart: 20,
    fontSize: FontSizes.medium,
    fontWeight: '400',
    color: Colors.black,
    letterSpacing: 1,
  },
});

export default styles;
