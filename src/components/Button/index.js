import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from './styles';
const Button = ({Title, buttonPress, bgColor, txtColor, opacity}) => (
  <TouchableOpacity
    onPress={buttonPress}
    style={[styles.mainContainer, {backgroundColor: bgColor}]}>
    <Text style={[styles.buttonText, {color: txtColor, opacity: opacity}]}>
      {Title}
    </Text>
  </TouchableOpacity>
);

export default Button;

Button.defaultProps = {
  Title: 'title',
  viewStyle: {},
  bgColor: '',
  buttonPress: () => {},
};
