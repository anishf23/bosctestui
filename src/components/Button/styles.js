import {StyleSheet, Platform} from 'react-native';
import {FontSizes, Colors, wp, hp} from '../../theme';

const styles = StyleSheet.create({
  mainContainer: {
    width: wp(90),
    height: hp(8.5),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 10,
    marginLeft: -10,
    marginTop: Platform.OS === 'android' ? 23 : 10,
  },
  buttonText: {
    color: Colors.theme,
    opacity: 0.4,
    fontSize: FontSizes.xLarge,
    fontWeight: '600',
    letterSpacing: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

export default styles;
