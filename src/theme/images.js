const Images = {
  backIcon: require('../assets/images/back_arrow.png'),
  lefticon: require('../assets/images/lefticon.png'),
  input: require('../assets/images/input.png'),
  password: require('../assets/images/password.png'),
  tabone: require('../assets/images/tabone.png'),
  tabtwo: require('../assets/images/tabtwo.png'),
  tabthree: require('../assets/images/tabthree.png'),
  tabfour: require('../assets/images/tabfour.png'),
  tabone_unselected: require('../assets/images/tabone_unselected.png'),
  tabtwo_unselected: require('../assets/images/tabtwo_unselected.png'),
  tabthree_unselected: require('../assets/images/tabthree_unselected.png'),
  tabfour_unselected: require('../assets/images/tabfour_unselected.png'),
  notificationwithbadge: require('../assets/images/notificationwithbadge.png'),
  notification: require('../assets/images/notification.png'),
  crossicon: require('../assets/images/crossicon.png'),
  charticon: require('../assets/images/charticon.png'),
};

export default Images;
