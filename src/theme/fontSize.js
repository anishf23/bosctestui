import {RFValue} from './responsive/responsiveFontSize';

const FontSizes = {
  xxSmall: RFValue(8),
  xSmall: RFValue(10),
  small: RFValue(12),
  medium: RFValue(14),
  large: RFValue(16),
  xLarge: RFValue(18),
  xxLarge: RFValue(20),
  smaller: RFValue(6),
  larger: RFValue(24),
  xlarger: RFValue(26),
  xxarger: RFValue(35),
  xxxarger: RFValue(40),
  xxxxarger: RFValue(45),
};

export default FontSizes;
