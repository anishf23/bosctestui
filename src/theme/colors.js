const Colors = {
  theme: '#150A45',
  white: '#FFFFFF',
  themeDark: '#7E170E',
  themeLight: '#E63223',
  black: '#000',
  unSelectedTab: '#757187',
  bgColor: '#F7F6F5',
  borderColor: 'rgba(21, 10, 69, 0.1)',
  buttonBgColor: '#E5E5E5',
  investButtonBgColor: '#5532FA',
  fullTransparant: '#FFFFFFFF',
  notiBgColor: 'rgba(247, 246, 245, 0.1)',
  notiTextColor: '#F7F6F5',
  chartColor: 'rgb(85, 50, 250,0.4)',
};

export default Colors;
