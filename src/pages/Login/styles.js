import {StyleSheet} from 'react-native';
import {FontSizes, Colors, hp, wp} from '../../theme';

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: Colors.theme,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainView: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    marginTop: 15,
  },
  rightArrowIcon: {
    width: wp(5),
    height: hp(5),
    alignItems: 'center',
  },
  pagerView: {
    width: '100%',
    height: '85%',
  },
  titleText: {
    color: Colors.white,
    fontSize: FontSizes.xxLarge,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  smallText: {
    color: Colors.unSelectedTab,
    fontSize: FontSizes.xSmall,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'left',
    marginTop: 10,
    letterSpacing: 1,
  },
  tabContainer: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: Colors.bgColor,
    alignItems: 'center',
    borderTopLeftRadius: 45,
    borderTopRightRadius: 45,
    marginTop: 80,
  },
  backTabview: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backIcon: {
    width: wp(8),
    height: hp(8),
    alignItems: 'center',
    tintColor: Colors.unSelectedTab,
    marginTop: 20,
    marginEnd: 15,
  },
  tabHeader: {
    width: wp(100),
    marginTop: 10,
  },
  textLargeBold: {
    color: Colors.theme,
    fontSize: FontSizes.xxarger,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'left',
    fontWeight: '600',
    marginTop: 10,
  },
  setUpView: {
    width: wp(85),
    flexDirection: 'column',
  },
  customInputview: {
    marginTop: 7,
  },
  smallThemeColorText: {
    color: Colors.theme,
    fontSize: FontSizes.large,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: '600',
    marginTop: 20,
    letterSpacing: 1,
  },
  bottomButtomView: {
    //flex: 1,
    flexDirection: 'column',
    //position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    // bottom: 0,
    // right: 0,
    // left: 0,
    marginBottom: 30,
  },
  viewCenter: {
    //flex: 1,
    height: hp(40),
    marginTop: 40,
  },
});

export default styles;
