// Static Data String
export const Investyourfuture = 'Invest in your future.';
export const EmailAddress = 'Email Address';
export const Password = 'Password';
export const Othersignoptions = 'Other sign in options';
export const Signin = 'Sign in';
export const emailvalidationmsg = 'Please enter your valid email';
export const passwordvalidationmsg = 'Please enter your password';
export const signinvalidationmsg = 'Sign in Successfully';
