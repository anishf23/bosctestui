import React, {useState} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {Images, Colors, wp} from '../../theme';
import * as data from './data';
import {TabHeader, InputView, Button} from '../../components';
import {AppLog, showSnack, strongRegex} from '../../utils/common';
import {YellowBox} from 'react-native';
YellowBox.ignoreWarnings(['']);
const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const signIn = () => {
    if (!strongRegex.test(email)) {
      showSnack(data.emailvalidationmsg);
    } else if (password.length <= 0) {
      showSnack(data.passwordvalidationmsg);
    } else {
      showSnack(data.signinvalidationmsg);
      navigation.reset({
        index: 0,
        routes: [{name: 'Main'}],
      });
    }
  };
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <StatusBar
        backgroundColor={Colors.theme}
        barStyle="dark-content"
        translucent
      />

      <View style={styles.sectionContainer}>
        <View style={styles.tabContainer}>
          <View style={styles.tabHeader} />
          <View style={styles.setUpView}>
            <TabHeader
              headerTitle={''}
              isImage={true}
              headerLeftIcon={Images.lefticon}
              onLeftIconPress={() => AppLog('onLeftIconPress pressed...')}
              onRightIconPress={() => AppLog('onRightIconPress pressed...')}
            />
            <Text style={styles.textLargeBold}>{data.Investyourfuture}</Text>

            <View style={styles.viewCenter}>
              <InputView
                viewStyle={styles.customInputview}
                editable={true}
                placeholderColor={Colors.unSelectedTab}
                txt={data.EmailAddress}
                onChangeText={text => setEmail(text)}
                keyboardType={'email-address'}
                isImage={true}
              />

              <InputView
                editable={true}
                txt={data.Password}
                onChangeText={text => setPassword(text)}
                secureTextEntry={true}
                isImage={true}
                placeholderColor={Colors.unSelectedTab}
                RightIcon={Images.password}
              />
            </View>
            <View style={styles.bottomButtomView}>
              <Button
                Title={data.Signin}
                buttonPress={() => signIn()}
                bgColor={Colors.investButtonBgColor}
                txtColor={Colors.white}
                opacity={1}
              />
              <TouchableOpacity>
                <Text style={styles.smallThemeColorText}>
                  {data.Othersignoptions}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};
export default Login;
