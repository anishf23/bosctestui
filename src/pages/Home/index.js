/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-unstable-nested-components */
import React, {useState, useRef,useEffect,} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
  Animated,
} from 'react-native';
import styles from './styles';
import {Images, Colors, wp, hp} from '../../theme';
import * as data from './data';
import {HomeHeader, TextLine} from '../../components';
import {AppLog} from '../../utils/common';
import PagerView from 'react-native-pager-view';
import {LineChart} from 'react-native-chart-kit';
const Home = ({navigation}) => {
  const [selected, setSelected] = useState(0);
  const [notificationShow, setNotificationShow] = useState(false);
  const {points, NotificationList} = data;
  const animatedOne =  useRef(new Animated.Value(0)).current

  useEffect(() => {
    const focusListener = navigation.addListener('focus', () => {
      console.log('onFocus');
      animatedOne.setValue(0)
      firstAnimation()
    });
    return () => {
      focusListener.remove();
    };


  }, []);

  const firstAnimation=()=>
  {
    Animated.timing(animatedOne, {
      toValue: wp(100),
      duration: data.duration,
      useNativeDriver: false,
    }).start(() => {
      console.log('Done1')   
         
    })
  }

  const TabTotal = () => {
    return (
      <View style={styles.toalTabContainer}>
        <View style={styles.setUpTotalView}>
          <Text style={styles.textSmall}>{data.RetirementSavings}</Text>
          <Text style={styles.textLargeBold}>{data.amount}</Text>
          <LineChartView />
          <Animated.View style={[{ transform: [{ translateX: animatedOne }] },styles.animatedView]}/>
        </View>
      </View>
    );
  };
  const LineChartView = () => {
    return (
      <LineChart
        data={{
          datasets: [
            {
              data: points,
            },
          ],
        }}
        fromZero={true}
        width={wp(100)}
        height={hp(40)}
        withInnerLines={false}
        withDots={false}
        withVerticalLines={false}
        withHorizontalLines={false}
        withVerticalLabels={false}
        withHorizontalLabels={false}
        withOuterLines={false}
        chartConfig={{
          backgroundColor: Colors.white,
          strokeWidth: 6,
          backgroundGradientFrom: Colors.white,
          backgroundGradientTo: Colors.white,
          color: (opacity = 0) => Colors.investButtonBgColor,
          style: {
            borderRadius: 5,
          },
          fillShadowGradientFrom: Colors.white,
          fillShadowGradientTo: Colors.white,
        }}
        bezier
        style={styles.chartViewConfig}
      />
    );
  };
  const renderItem = ({item}) => {
    return (
      <TouchableOpacity style={styles.rowItem}>
        <Text style={styles.notiSmall}>{item}</Text>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <StatusBar
        backgroundColor={Colors.theme}
        barStyle="dark-content"
        translucent
      />

      <View style={styles.sectionContainer}>
        <View
          style={[
            styles.notificationView,
            {display: notificationShow ? 'flex' : 'none'},
          ]}>
          <View style={styles.counterviewClose}>
            <Text style={styles.Countertext}>1/2</Text>
            <TouchableOpacity onPress={() => setNotificationShow(false)}>
              <Image
                style={[styles.crossIcon]}
                resizeMode="contain"
                source={Images.crossicon}
              />
            </TouchableOpacity>
          </View>
          <FlatList
            data={NotificationList}
            renderItem={renderItem}
            keyExtractor={item => item.id}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <View style={styles.setUpView}>
          <View style={styles.headerView}>
            <HomeHeader
              headerTitle={''}
              isImage={true}
              headerLeftIcon={Images.lefticon}
              headerRightIcon={
                notificationShow
                  ? Images.notification
                  : Images.notificationwithbadge
              }
              onLeftIconPress={() => AppLog('onLeftIconPress pressed...')}
              onRightIconPress={() => setNotificationShow(true)}
            />
          </View>

          <View style={styles.titleLineView}>
            <TextLine
              title={data.Total}
              selectUnSelectColor={
                selected === 0
                  ? Colors.investButtonBgColor
                  : Colors.fullTransparant
              }
              selectedTextColor={Colors.theme}
              linesize={wp(20)}
              isSelected={selected === 0 ? true : false}
              linePress={() => AppLog('linePressed...')}
            />
            <TextLine
              title={data.PreTax}
              selectUnSelectColor={
                selected === 1
                  ? Colors.investButtonBgColor
                  : Colors.fullTransparant
              }
              selectedTextColor={Colors.theme}
              linesize={wp(30)}
              isSelected={selected === 1 ? true : false}
              linePress={() => AppLog('linePressed...')}
            />
            <TextLine
              title={data.Roth}
              selectUnSelectColor={
                selected === 2
                  ? Colors.investButtonBgColor
                  : Colors.fullTransparant
              }
              selectedTextColor={Colors.theme}
              linesize={wp(18)}
              isSelected={selected === 2 ? true : false}
              linePress={() => AppLog('linePressed...')}
            />
          </View>
          <View style={styles.LineView} />
          <PagerView
            style={styles.pagerView}
            initialPage={0}
            onPageSelected={position => {
              setSelected(position.nativeEvent.position);
            }}>
            <TabTotal key="1" />
            <View style={styles.toalTabContainer} key="2">
              <Text style={styles.textLargeBold}>{data.PreTax}</Text>
            </View>
            <View style={styles.toalTabContainer} key="3">
              <Text style={styles.textLargeBold}>{data.Roth}</Text>
            </View>
          </PagerView>
        </View>
      </View>
    </SafeAreaView>
  );
};
export default Home;
