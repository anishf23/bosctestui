// Static Data String
export const Total = 'Total';
export const PreTax = 'Pre-Tax';
export const Roth = 'Roth';
export const RetirementSavings = 'RETIREMENT SAVINGS';
export const amount = '$16,561.14';
export const points = [25.7, 1, 19, 45, 15, 18, 80, 85, 50, 60, 60,100,90,25];
export const NotificationList = [
  "We're excited to announce that we've just launched a brand new feature that makes it easier than ever to track your retirement savings.",
  "We're excited to announce that we've just launched a brand new feature that makes it easier than ever to track your retirement savings.",
];
export const duration = 5000;
