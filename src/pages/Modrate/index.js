/* eslint-disable react-native/no-inline-styles */

import React, {useState} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {Images, Colors} from '../../theme';
import * as data from './data';
import {HomeHeader} from '../../components';
import {AppLog} from '../../utils/common';
import Pie from 'react-native-pie';
const Modrate = () => {
  const [notificationShow, setNotificationShow] = useState(false);
  const {
    NotificationList,
    pieChartList,
    radius,
    innerRadius,
    dividerSize,
    strokeCap,
  } = data;
  const renderItem = ({item}) => {
    return (
      <TouchableOpacity style={styles.rowItem}>
        <Text style={styles.notiSmall}>{item}</Text>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <StatusBar
        backgroundColor={Colors.theme}
        barStyle="dark-content"
        translucent
      />

      <View style={styles.sectionContainer}>
        <View
          style={[
            styles.notificationView,
            {display: notificationShow ? 'flex' : 'none'},
          ]}>
          <View style={styles.counterviewClose}>
            <Text style={styles.Countertext}>1/2</Text>
            <TouchableOpacity onPress={() => setNotificationShow(false)}>
              <Image
                style={[styles.crossIcon]}
                resizeMode="contain"
                source={Images.crossicon}
              />
            </TouchableOpacity>
          </View>
          <FlatList
            data={NotificationList}
            renderItem={renderItem}
            keyExtractor={item => item.id}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <View style={styles.setUpView}>
          <View style={styles.headerView}>
            <HomeHeader
              headerTitle={''}
              isImage={true}
              headerLeftIcon={Images.lefticon}
              headerRightIcon={
                notificationShow
                  ? Images.notification
                  : Images.notificationwithbadge
              }
              onLeftIconPress={() => AppLog('onLeftIconPress pressed...')}
              onRightIconPress={() => setNotificationShow(true)}
            />
            <View style={styles.setUpTotalView}>
              <Text style={styles.textSmall}>{data.YourPortfolio}</Text>
              <Text style={styles.textLargeBold}>
                {data.ModeratelyAggressive}
              </Text>
              <Text style={styles.changeSmall}>{data.Change}</Text>

              <Pie
                radius={radius}
                innerRadius={innerRadius}
                sections={pieChartList}
                dividerSize={dividerSize}
                strokeCap={strokeCap}
              />
              <View style={styles.pieCenterView}>
                <Text style={[styles.textLargeBold, styles.centerToViewText]}>
                  {data.reistPer + '\n'}
                  <Text style={[styles.cnterText]}>{data.RiskTolerance}</Text>
                </Text>
              </View>

              <View style={styles.bottomChartView}>
                <Text style={styles.smallText}>{data.Stocks}</Text>
                <Image
                  style={[styles.chartIcon]}
                  resizeMode="contain"
                  source={Images.charticon}
                />
                <Text style={styles.smallText}>{data.Bonds}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};
export default Modrate;
