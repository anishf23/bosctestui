// Static Data String
import {Colors} from '../../theme';
export const YourPortfolio = 'YOUR PORTFOLIO';
export const ModeratelyAggressive = 'Moderately Aggressive';
export const Change = 'Change';
export const RiskTolerance = 'Risk Tolerance';
export const Stocks = '75% Stocks';
export const Bonds = '25% Bonds';
export const radius = 140;
export const innerRadius = 130;
export const dividerSize = 6;
export const strokeCap = 'butt';
export const reistPer = '8.5';
export const pieChartList = [
  {
    percentage: 25,
    color: Colors.chartColor,
  },
  {
    percentage: 75,
    color: Colors.investButtonBgColor,
  },
];
export const NotificationList = [
  "We're excited to announce that we've just launched a brand new feature that makes it easier than ever to track your retirement savings.",
  "We're excited to announce that we've just launched a brand new feature that makes it easier than ever to track your retirement savings.",
];
