import {StyleSheet} from 'react-native';
import {FontSizes, Colors, hp, wp} from '../../theme';

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.white,
  },
  tabicon: {
    height: hp(8),
    width: wp(8),
  },
  textLargeBold: {
    color: Colors.investButtonBgColor,
    fontSize: FontSizes.xxxxarger,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'left',
    fontWeight: '600',
  },
  blankTab: {
    flexDirection: 'column',
    backgroundColor: Colors.white,
    height: hp(100),
    width: wp(100),
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
