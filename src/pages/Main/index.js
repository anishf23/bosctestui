/* eslint-disable react/no-unstable-nested-components */
import React from 'react';
import {SafeAreaView, View, Text, StatusBar, Image} from 'react-native';
import styles from './styles';
import * as data from './data';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Images, Colors, hp, wp} from '../../theme';
import Home from '../Home';
import Modrate from '../Modrate';
const Tab = createBottomTabNavigator();

const TabTwo = () => {
  return (
    <View style={styles.blankTab}>
      <Text style={styles.textLargeBold}>No Data</Text>
    </View>
  );
};
const Main = () => {
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <StatusBar
        backgroundColor={Colors.theme}
        barStyle="dark-content"
        translucent
      />

      <Tab.Navigator
        initialRouteName="Home"
        screenOptions={{
          tabBarActiveTintColor: Colors.investButtonBgColor,
          headerShown: false,
          tabBarShowLabel: false,
          tabBarStyle: {
            borderTopWidth: 0,
          },
        }}>
        <Tab.Screen
          name="Home"
          component={Home}
          options={{
            tabBarIcon: ({color, size, focused}) => (
              <Image
                style={[styles.tabicon, {tintColor: color}]}
                resizeMode="contain"
                source={focused ? Images.tabone : Images.tabone_unselected}
              />
            ),
          }}
        />
        <Tab.Screen
          name="tabtwo"
          component={Modrate}
          options={{
            tabBarIcon: ({color, size, focused}) => (
              <Image
                style={[styles.tabicon, {tintColor: color}]}
                resizeMode="contain"
                source={focused ? Images.tabtwo : Images.tabtwo_unselected}
              />
            ),
          }}
        />
        <Tab.Screen
          name="tabthree"
          component={TabTwo}
          options={{
            tabBarIcon: ({color, size, focused}) => (
              <Image
                style={[styles.tabicon, {tintColor: color}]}
                resizeMode="contain"
                source={focused ? Images.tabthree : Images.tabthree_unselected}
              />
            ),
          }}
        />

        <Tab.Screen
          name="tabfour"
          component={TabTwo}
          options={{
            tabBarIcon: ({color, size, focused}) => (
              <Image
                style={[styles.tabicon, {tintColor: color}]}
                resizeMode="contain"
                source={focused ? Images.tabfour : Images.tabfour_unselected}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </SafeAreaView>
  );
};
export default Main;
