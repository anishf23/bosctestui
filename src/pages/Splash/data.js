import {Dimensions} from 'react-native';

export const themeColor = '#0B0816';
export const colorAcent = '#5532FA';
export const deviceWidth = Dimensions.get('window').width;
export const waveViewHeight = 300;
export const colorWhite = '#fff';
export const waveValueOne = [25.7, 50, 30, 45, 40, 50, 80, 35, 50, 60, 60];
export const waveValueTwo = [97.5, 95, 120, 80, 95, 50, 65, 130, 100, 90, 100];
