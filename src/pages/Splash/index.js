/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState, useRef} from 'react';
import {
  SafeAreaView,
  StatusBar,
  View,
  Text,
  Animated,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {LineChart} from 'react-native-chart-kit';
import {
  themeColor,
  colorAcent,
  deviceWidth,
  waveViewHeight,
  waveValueOne,
  waveValueTwo,
} from './data';
import {AppLog} from '../../utils/common';

const Splash = ({navigation}) => {
  const scrollRef = useRef();
  const animatedOne = new Animated.Value(0);
  const animatedTwo = useRef(new Animated.Value(0)).current;
  const duration = 3000;
  let currentPosition = 0;
  let visisbleFirstAnimVie = true;
  const [visisbleSecondAnimVie, setVisibleSecondAnimView] = useState(true);
  const [visisbleButtons, setVisisbleButtons] = useState(false);
  const timerRef = useRef(null);

  useEffect(() => {
    firstAnimation();

    return () => {
      cleareTimer();
    };
  }, []);
  const cleareTimer = () => {
    console.log('inside cleareTimer');
    clearInterval(timerRef.current);
  };
  const openNextScreen = () => {
    navigation.replace('Register');
  };

  const firstAnimation = () => {
    Animated.timing(animatedOne, {
      toValue: 150,
      duration: duration,
      useNativeDriver: false,
    }).start(() => {
      console.log('Done1');
      visisbleFirstAnimVie = false;
      setVisisbleButtons(true);
    });
  };

  const secondAnimation = () => {
    setVisisbleButtons(false);
    Animated.timing(animatedTwo, {
      toValue: deviceWidth / 4 + 18,
      duration: duration,
      useNativeDriver: false,
    }).start(() => {
      console.log('Done2');
      setVisibleSecondAnimView(false);
      scrollScrollingView();
    });
  };
  const scrollScrollingView = () => {
    timerRef.current = setInterval(() => {
      // AppLog('','timer')
      var position = currentPosition + 5; // x decides the speed and currentPosition is set to 0 initially.
      scrollRef.current?.scrollTo({x: position, animated: true});
      currentPosition = position;
    }, 50);
  };

  const animatedViewOne = () => {
    return (
      <Animated.View
        style={[
          {transform: [{translateX: animatedOne}]},
          styles.animatedViewOne,
        ]}
      />
    );
  };

  const animatedViewTwo = () => {
    return (
      <Animated.View
        style={[
          {transform: [{translateX: animatedTwo}]},
          styles.animatedViewTwo,
        ]}
      />
    );
  };
  //   const isCloseToEnd = ({ layoutMeasurement, contentOffset, contentSize }) => {
  //     return layoutMeasurement.width + contentOffset.x >=
  //       contentSize.width;
  //   };

  const isCloseToEnd = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToRight = 20;
    return (
      layoutMeasurement.width + contentOffset.x >=
      contentSize.width - paddingToRight
    );
  };

  const waveView = (value, style) => {
    return (
      <LineChart
        data={{
          datasets: [
            {
              data: value,
            },
          ],
        }}
        fromZero={true}
        width={deviceWidth} // from react-native
        height={waveViewHeight}
        withInnerLines={false}
        withDots={false}
        withVerticalLines={false}
        withHorizontalLines={false}
        withVerticalLabels={false}
        withHorizontalLabels={false}
        chartConfig={{
          backgroundColor: themeColor,
          backgroundGradientFrom: themeColor,
          backgroundGradientTo: themeColor,
          color: () => colorAcent,
          labelColor: () => themeColor,
          style: styles.waveLineStyle,
          fillShadowGradientFrom: themeColor,
          fillShadowGradientTo: themeColor,
        }}
        bezier
        style={style}
      />
    );
  };

  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <StatusBar backgroundColor={themeColor} barStyle="dark-content" />
      <View style={styles.mainView} resizeMode={'cover'}>
        <Animated.ScrollView
          showsHorizontalScrollIndicator={false}
          decelerationRate={'normal'}
          disableIntervalMomentum={true}
          scrollEventThrottle={16}
          onScroll={({nativeEvent}) => {
            if (isCloseToEnd(nativeEvent)) {
              cleareTimer();
              openNextScreen();
            }
          }}
          horizontal={true}
          style={styles.scrollViewStyle}
          ref={scrollRef}
          scrollEnabled={false}
          scrollToOverflowEnabled={true}>
          <View style={styles.textAndWaveContainer}>
            <View
              style={{
                flexDirection: 'row',
                marginStart: deviceWidth / 2 - 40,
              }}>
              <Text style={styles.textG}>g</Text>
              <View style={styles.verticalLine} />
            </View>
            <Text style={styles.udeLine}>uideline</Text>
            {visisbleFirstAnimVie ? animatedViewOne() : null}
            <View style={styles.waveContainer}>
              {waveView(waveValueOne, styles.waveStyle1)}
              {/* {waveView(waveValueTwo, styles.waveStyle2)} */}
              {visisbleSecondAnimVie ? animatedViewTwo() : null}
            </View>
            <View style={styles.dummyView} />
          </View>
        </Animated.ScrollView>
        {visisbleButtons ? (
          <View style={styles.buttonsContainer}>
            <TouchableOpacity
              style={styles.btnActivate}
              onPress={() => {
                secondAnimation();
              }}>
              <Text style={styles.txtWhite}>Activate Account</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                secondAnimation();
              }}
              style={styles.btnSignIn}>
              <Text style={styles.txtWhite}>Sign In</Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    </SafeAreaView>
  );
};
export default Splash;
