// Static Data String
export const welcomekatie = 'Welcome \nKatie!';
export const welcomeGuideline =
  'Welcome to the Guideline 401(k) for Intuit.To get started, let’s setup your account and verify your personal information.';
export const Intuit = 'Intuit';
export const Workemail = 'Work email';
export const Personalemail = 'Personal email';
export const SaveContinue = 'Save & Continue';
export const dontparticipate = 'I don’t want to participate.';
export const second = 'Welcome \nSteps 2';
export const thired = 'Welcome \nSteps 3';
export const four = 'Welcome \nSteps 4';
