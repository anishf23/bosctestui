import React, {useState} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {Images, Colors, wp} from '../../theme';
import * as data from './data';
import PagerView from 'react-native-pager-view';
import {StepsLine, TabHeader, InputView, Button} from '../../components';
import {AppLog} from '../../utils/common';
import {YellowBox} from 'react-native';
YellowBox.ignoreWarnings(['']);
const TabRegister = ({navigation}) => {
  return (
    <View style={styles.tabContainer} key="1">
      <View style={styles.tabHeader}>
        <TabHeader
          headerTitle={''}
          isImage={false}
          headerLeftIcon={Images.lefticon}
          headerRightIcon={Images.lefticon}
          onLeftIconPress={() => AppLog('onLeftIconPress pressed...')}
          onRightIconPress={() => AppLog('onRightIconPress pressed...')}
        />
      </View>
      <View style={styles.setUpView}>
        <Text style={styles.textLargeBold}>{data.welcomekatie}</Text>
        <Text style={styles.smallText}>{data.welcomeGuideline}</Text>

        <InputView
          editable={true}
          txt={data.Intuit}
          onChangeText={text => AppLog('onChangeText...', text)}
          secureTextEntry={true}
          isImage={true}
          placeholderColor={Colors.black}
          RightIcon={Images.input}
        />
        <InputView
          viewStyle={styles.customInputview}
          editable={true}
          placeholderColor={Colors.unSelectedTab}
          txt={data.Workemail}
          onChangeText={text => AppLog('onChangeText...', text)}
          keyboardType={'email-address'}
          isImage={true}
        />

        <InputView
          viewStyle={styles.customInputview}
          editable={true}
          placeholderColor={Colors.unSelectedTab}
          txt={data.Personalemail}
          onChangeText={text => AppLog('onChangeText...', text)}
          keyboardType={'email-address'}
          isImage={true}
        />
        <Button
          Title={data.SaveContinue}
          buttonPress={() => navigation.navigate('Login')}
          bgColor={Colors.buttonBgColor}
          txtColor={Colors.theme}
          opacity={0.4}
        />
        <TouchableOpacity>
          <Text style={styles.smallThemeColorText}>{data.dontparticipate}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const TabSecond = () => {
  return (
    <View style={styles.tabContainer} key="1">
      <View style={styles.tabHeader}>
        <TabHeader
          headerTitle={''}
          isImage={false}
          headerLeftIcon={Images.lefticon}
          headerRightIcon={Images.lefticon}
          onLeftIconPress={() => AppLog('onLeftIconPress pressed...')}
          onRightIconPress={() => AppLog('onRightIconPress pressed...')}
        />
      </View>
      <View style={styles.setUpView}>
        <Text style={styles.textLargeBold}>{data.second}</Text>
      </View>
    </View>
  );
};
const Tabthired = () => {
  return (
    <View style={styles.tabContainer} key="1">
      <View style={styles.tabHeader}>
        <TabHeader
          headerTitle={''}
          isImage={false}
          headerLeftIcon={Images.lefticon}
          headerRightIcon={Images.lefticon}
          onLeftIconPress={() => AppLog('onLeftIconPress pressed...')}
          onRightIconPress={() => AppLog('onRightIconPress pressed...')}
        />
      </View>
      <View style={styles.setUpView}>
        <Text style={styles.textLargeBold}>{data.thired}</Text>
      </View>
    </View>
  );
};
const TabFour = () => {
  return (
    <View style={styles.tabContainer} key="1">
      <View style={styles.tabHeader}>
        <TabHeader
          headerTitle={''}
          isImage={false}
          headerLeftIcon={Images.lefticon}
          headerRightIcon={Images.lefticon}
          onLeftIconPress={() => AppLog('onLeftIconPress pressed...')}
          onRightIconPress={() => AppLog('onRightIconPress pressed...')}
        />
      </View>
      <View style={styles.setUpView}>
        <Text style={styles.textLargeBold}>{data.four}</Text>
      </View>
    </View>
  );
};

const Register = ({navigation}) => {
  const [selected, setSelected] = useState(0);
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <StatusBar
        backgroundColor={Colors.theme}
        barStyle="dark-content"
        translucent
      />

      <View style={styles.sectionContainer}>
        <View style={styles.backTabview}>
          <Image
            style={[styles.backIcon]}
            resizeMode="contain"
            source={Images.backIcon}
          />
          <StepsLine
            selectUnSelectColor={
              selected === 0 ? Colors.white : Colors.unSelectedTab
            }
          />
          <StepsLine
            selectUnSelectColor={
              selected === 1 ? Colors.white : Colors.unSelectedTab
            }
          />
          <StepsLine
            selectUnSelectColor={
              selected === 2 ? Colors.white : Colors.unSelectedTab
            }
          />
          <StepsLine
            selectUnSelectColor={
              selected === 3 ? Colors.white : Colors.unSelectedTab
            }
          />
        </View>

        <PagerView
          style={styles.pagerView}
          initialPage={0}
          onPageSelected={position => {
            setSelected(position.nativeEvent.position);
          }}>
          <TabRegister navigation={navigation} key="1" />
          <TabSecond key="2" />
          <Tabthired key="3" />
          <TabFour key="4" />
        </PagerView>
      </View>
    </SafeAreaView>
  );
};
export default Register;
